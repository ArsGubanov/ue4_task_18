﻿#include<iostream>

using namespace std;

class Stack
{
private:
	int* stack;
	int* dummy;
	int top, max;

	int increaseStack()
	{
		dummy = stack;
		stack = new int[max + 1];
		for (int i = 0; i < max; i++)
		{
			stack[i] = dummy[i];
		}
		max = max + 1;
		delete[] dummy;
		return 0;
	}


public:
	Stack()
	{
		stack = new int[1];
		top = -1;
		max = 1;
	}

	int push(int value)
	{
		if (top >= max - 1)
		{
			increaseStack();
		}
		top = top + 1;
		stack[top] = value;
		

		return 0;
	}

	int pop()
	{
		if (top > -1)
		{
			top = top - 1;
			return 0;
		}
		cout << "Stack has no values!" << endl;
		return 0;
	}



	int print()
	{
		for (int i = 0; i <= top;i++) {
			cout << stack[i] << endl;
		}
		return 0;
	}

};


int main()
{
	Stack st;
	st.pop();
	st.push(2);
	st.push(6);
	st.pop();
	st.push(7);
	st.push(1);
	st.pop();
	st.push(3);
	st.push(8);
	st.print();
	return 0;
}